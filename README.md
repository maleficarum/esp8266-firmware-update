# Introduction #

This module allows the ESP8266 compatible device to ask for an update to a server, which knows whether the device already has a firmaware or not.

The projects contains two pieces of code:

* The ESP8266 library
* The backend nodejs module

### ESP8266 library ###

This library has to be included in the arduino IDE; see the Download section on the repository to get it. Once you've installed on Arduino IDE the usage is pretty simple:

```cpp
#include <ESP8266Autoupdate.h>
#include <ESP8266WiFi.h>

const char* ssid     = "your-ssid";
const char* password = "your-password";

ESP8266Autoupdate updater = ESP8266Autoupdate(10, "http://localhost:3000", "fan-controller", true);

void setup() {
  WiFi.begin(ssid, password);
  pinMode(LED_BUILTIN, OUTPUT);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }  
}

void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second

   updater.loop();
}
```

The arguments are as follows:

* interval : The update check interval in seconds; how often de device will ask for a update.
* url : The endpoint that will serve the firmware.
* deviceId : The device id. This is used to get track of the updates over your devices. Even if you have a specific firmware for a specific device.
* restartOnUpdate : The devices should restart after firmware update ?

### The server side ###

You need to write a simple code to create a server that listen for firmware update requests.

#### Install

```bash
$ npm install esp8266-firmware-update --save
```

#### Usage

```javascript
const updateServer = require("esp8266-firmware-update");

updateServer.configure({
  "port":3000,
  "firmware":"firmware.bin",
  "firmwareLocation":"/opt/firmware",
  "dbLocation":"/opt/db/"
});
```

Also, you can override the configuration options passing the flags:

```bash

node app.js --firmware=firmware.bin --firmware-location=/opt/firmware

```

Or even you can use the default firmware location (./firmware):

```bash

node app.js --firmware=firmware.bin

```

Or even you can use the default firmware name (firmware.bin):

```bash

node app.js --firmware-location=/opt/firmware

```

also, the module may keep in memory (as default option) the firmware definition and the flashing history, meaning that the data will lost on reboot. If you want to keep all the operation you have to specify the db location:

```bash

node app.js --db-location=/top/db/

```
And, if you need to flash a specific firmware to a specific device you can to name your firmware file with the Particle device id, asumming your device has the ID ABCDE1234ABCDE1234ABCDE1234:

```bash

ABCDE1234ABCDE1234ABCDE1234ABCDE1234.bin

```
 so you can diferentiate which firmware goes to which device.

#### Start

Start listening for events

```javascript

p.start();

```

If you are going to include the module on an existing express app, you can bind the module to the these app.

```javascript
//Assuming you have defined an express app as follows
const express = require("express");
var app = express();

p.start(app);

app.listen(3001, function () {
  console.log('Update Service listening on port ');
});

```

#### Logging

This module uses bunyan as a logger; if you want to provide custom logger configuration now you can (since version 1.1.14) pass it as a config parameter in a bunyan config fashion :

```javascript
const updateServer = require("esp8266-firmware-update");

updateServer.configure({
  "port":3000,
  "firmware":"firmware.bin",
  "firmwareLocation":"/opt/firmware",
  "dbLocation":"/opt/db/",
  "logger": {
    name: "MyCustomLoggerConfig",
    src: false,
    streams: [
      {
        level:'debug',
        stream: process.stdout
      }
    ]
  }
});
```

[For more bunyan options go to bunyan documentation](https://github.com/trentm/node-bunyan "Bunyan documentation")


#### Command line options

- firmware : (Optional. Default firmware.bin). The name of your latest firmware to flash.
- firmware-location : (Optional. Default ./firmware). The directory where the module will look for a firmware file.
- db-location : (Optional). If you specify this path, your DB will be on memory, otherwise the database will be stored in that location.
- no-ui : If passed, no web UI will be available.

#### JSON Config options

- port : (Mandatory). The HTTP port that the module will listen for requests.
- firmware : (Optional. Default firmware.bin). The name of your latest firmware to flash.
- firmwareLocation : (Optional. Default ./firmware). The directory where the module will look for a firmware file.
- dbLocation : (Optional). If you specify this path, your DB will be on memory, otherwise the database will be stored in that location.


If the project is useful for you ... buy me a coffee

[![Buy me a coffee](https://az743702.vo.msecnd.net/cdn/kofi3.png?v=0 "Buy me a coffee")](https://ko-fi.com/Q5Q569GK)
