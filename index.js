'use strict';
/**
* author : maleficarum [ maleficarum.mx ]
*/
const bunyan = require('bunyan');
const argv = require('minimist')(process.argv.slice(2));
const exphbs  = require('express-handlebars');
const dateFormat = require('dateformat');
const fs = require("fs");
const md5File = require('md5-file');
const Datastore = require('nedb');
const express = require('express');
const path = require("path");
const pack = require('./package.json');
var app = express();

var config;
var flashList = [];
var db = {};

const FIRMWARE_LOCATION = './firmware';
const FIRMWARE_NAME = "firmware.bin";

var log = bunyan.createLogger({
  name: "ESP8266Autoupdate",
  src: false,
  streams: [
    {
      level:'debug',
      stream: process.stdout
    }
  ]
});

module.exports = {
  configure: function(c) {
    config = c || {};

    if(c.logger != undefined) {
      log = bunyan.createLogger(c.logger);

      log.info("This log has been created with custom params ...");
    }

    log.info("ESP8266 updater version " + pack.version);

    if(argv["firmware-location"] != undefined) {
      config.firmwareLocation = argv["firmware-location"];
      log.info("Using command line firmware location argument : " + config.firmwareLocation);
    } else if(config.firmwareLocation != undefined){
      log.info("Using firmware location as " + config.firmwareLocation);
    } else {
      config.firmwareLocation = FIRMWARE_LOCATION;
      log.info("Using default firmware location as " + config.firmwareLocation);
    }

    //Verify if the firmware location exists.
    try {
      fs.statSync(config.firmwareLocation);
    } catch(e) {
      log.warn("Creating " + config.firmwareLocation + " cause doesn't exist");
      fs.mkdirSync(config.firmwareLocation);
    }

    //Always we'll look for this file, otherwise we'll look at the database
    if(argv["firmware"] != undefined ) {
      config.firmware = argv["firmware"];
      log.info("Using firmware " + config.firmware);
    }

    if(argv["db-location"] != null || config.dbLocation != null) {
      config.dbLocation = argv["db-location"] || config.dbLocation;

      //Verify if the firmware location exists.
      try {
        fs.statSync(config.dbLocation);
      } catch(e) {
        log.warn("Creating " + config.dbLocation + " cause doesn't exist");
        fs.mkdirSync(config.dbLocation);
      }

      db.device = new Datastore({ filename: config.dbLocation + "/device.db", autoload: true });
      db.firmware = new Datastore({ filename: config.dbLocation + "/firmware.db", autoload: true });
      log.info("Creating a persistent store in " + config.dbLocation);

    } else {
      log.info("Creating a in-memory database");
      db.device = new Datastore();
      db.firmware = new Datastore();
    }
  },
  start: function(customApp) {
    fs.watch(config.firmwareLocation, (eventType, filename) => {
      const path = config.firmwareLocation + "/" + filename;

      fs.stat(path, function(err, stat) {
        if(err) {//The firmware doesn't exist anymore
          db.firmware.remove({ "name": filename }, { multi: true }, function (err, numRemoved) {
            log.info("Removed " + numRemoved + " firmware definitions");
          });
        } else {
          if(stat.isDirectory()) {
            log.debug("New directory created " + filename);
            return;
          }
          log.info("New firmware detected " + filename);
          const hash = md5File.sync(path);

          //Check whether is the same firmware or it's different even with the same name
          db.firmware.findOne({ "name": filename, "hash":hash }, function (err, device) {
            if(device) {
              log.warn("The firmware " + path + " already exists in the database.")
            } else {
              db.firmware.insert({"name": filename, "hash":hash, "version": 1, "default": true, createdAt: new Date()}, function (err, document) {
                if(err) {
                  log.error("Error saving new firmware", err);
                } else {
                  log.debug("Saved new firmware definition : " + filename);
                }
              });
            }
          });
        }
      });
    });

    if(argv["no-ui"]  == undefined) {
      if(customApp != undefined) {
        app = customApp;
        initWebApp();
        log.info("Using custom express app");
      } else {
        app.listen(3000, function () {
          log.info('Update Service listening on port ' + config.port);
        });
        initWebApp();
      }
    } else {
      log.warn("No UI started.");
    }

    app.get('/dh7/esp8266/update', function(req, res) {
      //If we are going to use the default firmware we dont have to look at database for the
      //default firmware, otherwise we have to look at database for the custom firmware
      //This overwrite the saved and the last firmware for the given default firmware.
      if(config.firmware != undefined) {
        log.debug("Firmware name found on config.");
        fetchDevice(req.query.deviceId, config.firmware, req, res, function() {

        });
      } else {
        log.debug("Firmware name not set. Looking on the database.");
        db.firmware.find({}).sort({"registrationDate":1}).exec(function(err, firmwareList) {
          var len = Object.keys(firmwareList).length;

          if(len == 0) {
            log.error("No firmware found for device " + req.query.deviceId);
            res.status(404).json({"message":"Firmware not found."});
          } else {
            const firmware = firmwareList[0];

            fetchDevice(req.query.deviceId, firmware.name, req, res, function() {
              //console.log("Found registrered");
            });
          }
        });
      }
    });
  }
};

var fetchDevice = function(deviceId, firmware, req, res, callback) {
  var path = config.firmwareLocation + "/" + deviceId + ".bin";

  //Look for the firmware with the same name of the device
  fs.stat(path, function(err, stat) {
    if(err) {
      path = config.firmwareLocation + "/" + firmware;
    } else {
      log.debug("Using custom firmware '" + deviceId + ".bin' for device " + deviceId);
    }

    fs.stat(path, function(err, stat) {
      if(err) {
        log.warn("No firmware found ...");
        return;
      }

      const hash = md5File.sync(path);
      //Check for each device
      db.device.findOne({ device: deviceId, firmwareHash: hash }, function (err, device) {
        if(device == undefined) {
          log.info("Device " + deviceId + " doesn't have this firmware");
          flashDevice(deviceId, path, req, res);
          callback();
        } else {
          log.warn("Device " + deviceId + " already flashed");
          res.status(404).json({ "message" : "Device already flashed" });
        }
      });
    });
  });
};

var flashDevice = function(device, firmware, req, res) {
  const hash = md5File.sync(firmware);
  var file = fs.createReadStream(firmware);
  var stat = fs.statSync(firmware);

  res.setHeader('Content-type', 'application/octet-stream');
  res.setHeader('Content-disposition', 'attachment; filename="firmware.bin');
  res.setHeader('Content-Transfer-Encoding', 'binary');
  res.setHeader('Content-Length', stat.size);

  file.pipe(res);

  db.device.insert({"device": device, flashedAt: new Date(), firmwareHash: hash}, function (err, document) {
    if(err) {
      log.error("Error saving device flash process ", err);
    } else {
      log.info('Device flashing started successfully ', device);
    }
  });
};

var initWebApp = function() {
  app.engine('handlebars', exphbs({defaultLayout: 'main', layoutsDir: path.join(__dirname, 'views/layouts')}));
  app.set('view engine', 'handlebars');

  app.use(express.static(path.join(__dirname, 'public')));
  app.set('views', path.join(__dirname, 'views'));

  app.get('/', adminWebRoot);
};

var adminWebRoot = function(req, res) {
  //Fetch all the firmware
  db.firmware.find({}).sort({"registrationDate":1}).exec(function(err, firmwareList) {
      res.render('index', { title: 'Wemos Update Site', firmwareList: firmwareList });
  });
};
