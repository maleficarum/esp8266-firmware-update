#include <ESP8266Autoupdate.h>
#include <ESP8266httpUpdate.h>

ESP8266Autoupdate::ESP8266Autoupdate(int interval, String host, String deviceId, bool autoRestart) {
  _interval = interval;
  _host = host;
  _autoRestart = autoRestart;
  _deviceId = deviceId;
  _lastUpdate = millis();
}

void ESP8266Autoupdate::loop() {
  if((millis() - _lastUpdate) >= _interval * 1000) {
    Serial.print("Going to check for update at ");
    Serial.println(_host + "/dh7/esp8266/update?deviceId=" + _deviceId);

    _lastUpdate = millis();

    ESPhttpUpdate.rebootOnUpdate(_autoRestart);
    t_httpUpdate_return ret = ESPhttpUpdate.update(_host + "/dh7/esp8266/update?deviceId=" + _deviceId);
    switch(ret) {
      case HTTP_UPDATE_FAILED:
        Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
        break;

      case HTTP_UPDATE_NO_UPDATES:
        Serial.println("HTTP_UPDATE_NO_UPDATES");
        break;

      case HTTP_UPDATE_OK:
        Serial.println("HTTP_UPDATE_OK");
        break;
    }
  }
}
