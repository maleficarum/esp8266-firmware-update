#ifndef ESP8266Autoupdate_h
#define ESP8266Autoupdate_h

#include "Arduino.h"

class ESP8266Autoupdate {

  public:
    ESP8266Autoupdate(int interval, String host, String deviceId, bool autoRestart);
    void loop();
  private:
    int _interval;
    String _host;
    bool _autoRestart;
    long _lastUpdate;
    String _deviceId;
};

#endif
